import ProductUIServiceBase from './product-ui-service-base';

/**
 * 产品UI服务对象
 *
 * @export
 * @class ProductUIService
 */
export default class ProductUIService extends ProductUIServiceBase {

    /**
     * Creates an instance of  ProductUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProductUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}