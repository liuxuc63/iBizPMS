import FileUIServiceBase from './file-ui-service-base';

/**
 * 附件UI服务对象
 *
 * @export
 * @class FileUIService
 */
export default class FileUIService extends FileUIServiceBase {

    /**
     * Creates an instance of  FileUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  FileUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}