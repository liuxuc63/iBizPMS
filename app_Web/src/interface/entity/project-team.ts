/**
 * 项目团队
 *
 * @export
 * @interface ProjectTeam
 */
export interface ProjectTeam {

    /**
     * 加盟日
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    join?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    account?: any;

    /**
     * 可用工时/天
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    hours?: any;

    /**
     * 预计剩余
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    left?: any;

    /**
     * 可用工日
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    days?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    id?: any;

    /**
     * 总计消耗
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    consumed?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    order?: any;

    /**
     * 最初预计
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    estimate?: any;

    /**
     * 受限用户
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    limited?: any;

    /**
     * 角色
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    role?: any;

    /**
     * 团队类型
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    type?: any;

    /**
     * 总计可用
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    total?: any;

    /**
     * 项目编号
     *
     * @returns {*}
     * @memberof ProjectTeam
     */
    root?: any;
}