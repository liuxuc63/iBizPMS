export default {
  fields: {
    branch: "平台/分支",
    createman: "建立人",
    updatedate: "更新时间",
    productlifename: "产品生命周期名称",
    product: "产品",
    createdate: "建立时间",
    parent: "父对象",
    year: "年",
    updateman: "更新人",
    type: "属性",
    marker: "里程碑",
    begin: "开始日期",
    productlifeid: "产品生命周期标识",
    end: "结束日期",
  },
	views: {
		roadmaplistview: {
			caption: "产品生命周期",
      		title: "路线图",
		},
		roadmaplistview9: {
			caption: "产品生命周期",
      		title: "路线图",
		},
	},
};